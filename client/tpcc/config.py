import json
import os
import sys

from . import helpers
from .echo import echo
from .exceptions import ClientError

TEMPLATE = {
    "solutions": {
        "path": None,
        "url": None,
    },
    "gitlab": {
        "user": None,
        "oauth": None,
    },
    "labels": []
}


class Config(object):
    def __init__(self, git_repo):
        self.path = self._path(git_repo.working_tree_dir)
        self._init()

    @staticmethod
    def _path(repo_root_dir):
        return os.path.join(repo_root_dir, "client/.config")

    def _init(self):
        if not os.path.exists(self.path):
            self._create_new()

        self._load()

    def _create_new(self):
        self._save(TEMPLATE)

    def _load(self):
        self.data = helpers.load_json(self.path)

    def _save(self, data):
        with open(self.path, 'w') as f:
            json.dump(data, f, indent=4, sort_keys=True)
        self.data = data

    def show(self):
        echo.echo("Config [{}]:".format(self.path))
        echo.write(json.dumps(self.data, indent=4, sort_keys=True))

    def patch(self, updates):
        self.data.update(updates)
        self._save(self.data)

    def get(self, path):
        keys = path.split('.')
        data = self.data
        for key in keys:
            if key not in data:
                raise ClientError(
                    "Path not found in task config: '{}'".format(path))
            data = data[key]

        if data is None:
            raise ClientError("Config attribute '{}' not set".format(path))

        return data

    def set_attribute(self, path, value):
        keys = path.split('.')

        data = self.data
        for key in keys[:-1]:
            if key not in data:
                raise ClientError(
                    "Path not found in task config: '{}'".format(path))
            data = data[key]

        key = keys[-1]
        if key not in data:
            raise ClientError(
                "Path not found in task config: '{}'".format(path))

        data[key] = value

        self._save(self.data)
